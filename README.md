
# [Advance Leetcode] User Interconnect Service

A application to demo how to connect back-end services (register and login and view user) to front-end, involving login & register + views

## Microservices List

1. [This] User Interconnect Service (API / Back-End)
2. [User Interconnect Service](https://gitlab.com/redevartk.advance-leetcode/user-interconnect) : For Views / Front-End

## Properties

* Framework 		: CodeIgniter 4.1.8
* Programming Lang	: PHP 8.1.0
* Database			: MySQL 8.0.30

## Developer Note

* This is an old app using old method,
* For unknown reason the API_KEY doesn't work
* It have cors problem for now
* I don't intend to continue this project
* Mostly used for basic knowledge of interconnecting both front-end and back-end
